import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class JamesBondTest {
	
	jamesBond bond;
	boolean jamesBond;

	@BeforeEach
	void setUp() throws Exception {
		bond = new jamesBond();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	
	@Test
	public void testCase1() {
	    assertFalse(bond.bondRegex("("));
	}

	@Test
	public void testCase2() {
	    assertFalse(bond.bondRegex("( ("));
	}

	@Test
	public void testCase3() {
	    assertFalse(bond.bondRegex("( ( ("));
	}

	@Test
	public void testCase4() {
	    assertFalse(bond.bondRegex("( ( )"));
	}

	@Test
	public void testCase5() {
	    assertTrue(bond.bondRegex("( ( 0 0 7 )"));
	}

	@Test
	public void testCase6() {
	    assertFalse(bond.bondRegex("( ( 0 7 )"));
	}

	@Test
	public void testCase7() {
	    assertFalse(bond.bondRegex("( ( 7 )"));
	}

	@Test
	public void testCase8() {
	    assertFalse(bond.bondRegex("( )"));
	}

	@Test
	public void testCase9() {
	    assertFalse(bond.bondRegex("( ) ("));
	}

	@Test
	public void testCase10() {
	    assertFalse(bond.bondRegex("( ) )"));
	}

	@Test
	public void testCase11() {
	    assertTrue(bond.bondRegex("( ) 0 0 7 )"));
	}

	@Test
	public void testCase12() {
	    assertFalse(bond.bondRegex("( ) 0 7 )"));
	}

	@Test
	public void testCase13() {
	    assertFalse(bond.bondRegex("( ) 7 )"));
	}

	@Test
	public void testCase14() {
	    assertFalse(bond.bondRegex("( 0 ("));
	}

	@Test
	public void testCase15() {
	    assertFalse(bond.bondRegex("( 0 ( ("));
	}

	@Test
	public void testCase16() {
	    assertFalse(bond.bondRegex("( 0 ( )"));
	}

	@Test
	public void testCase17() {
	    assertTrue(bond.bondRegex("( 0 ( 0 0 7 )"));
	}

	@Test
	public void testCase18() {
	    assertFalse(bond.bondRegex("( 0 ( 0 7 )"));
	}

	@Test
	public void testCase19() {
	    assertFalse(bond.bondRegex("( 0 ( 7 )"));
	}

	@Test
	public void testCase20() {
	    assertFalse(bond.bondRegex("( 0 )"));
	}

	@Test
	public void testCase21() {
	    assertFalse(bond.bondRegex("( 0 ) ("));
	}

	@Test
	public void testCase22() {
	    assertFalse(bond.bondRegex("( 0 ) )"));
	}

	@Test
	public void testCase23() {
	    assertFalse(bond.bondRegex("( 0 ) 0 0 7 )"));
	}

	@Test
	public void testCase24() {
	    assertFalse(bond.bondRegex("( 0 ) 0 7 )"));
	}

	@Test
	public void testCase25() {
	    assertFalse(bond.bondRegex("( 0 ) 7 )"));
	}

	@Test
	public void testCase26() {
	    assertFalse(bond.bondRegex("( 0 0 ("));
	}

	@Test
	public void testCase27() {
	    assertFalse(bond.bondRegex("( 0 0 ( ("));
	}

	@Test
	public void testCase28() {
	    assertFalse(bond.bondRegex("( 0 0 ( )"));
	}

	@Test
	public void testCase29() {
	    assertTrue(bond.bondRegex("( 0 0 ( 0 0 7 )"));
	}

	@Test
	public void testCase30() {
	    assertFalse(bond.bondRegex("( 0 0 ( 0 7 )"));
	}

	@Test
	public void testCase31() {
	    assertFalse(bond.bondRegex("( 0 0 ( 7 )"));
	}

	@Test
	public void testCase32() {
	    assertFalse(bond.bondRegex("( 0 0 )"));
	}

	@Test
	public void testCase33() {
	    assertFalse(bond.bondRegex("( 0 0 ) ("));
	}

	@Test
	public void testCase34() {
	    assertFalse(bond.bondRegex("( 0 0 ) )"));
	}

	@Test
	public void testCase35() {
	    assertFalse(bond.bondRegex("( 0 0 ) 0 0 7 )"));
	}

	@Test
	public void testCase36() {
	    assertFalse(bond.bondRegex("( 0 0 ) 0 7 )"));
	}

	@Test
	public void testCase37() {
	    assertFalse(bond.bondRegex("( 0 0 ) 7 )"));
	}

	@Test
	public void testCase38() {
	    assertFalse(bond.bondRegex("( 0 0 0 ("));
	}

	@Test
	public void testCase39() {
	    assertFalse(bond.bondRegex("( 0 0 0 )"));
	}

	@Test
	public void testCase40() {
	    assertFalse(bond.bondRegex("( 0 0 0 0 0 7 )"));
	}

	@Test
	public void testCase41() {
	    assertFalse(bond.bondRegex("( 0 0 0 0 7 )"));
	}

	@Test
	public void testCase42() {
	    assertFalse(bond.bondRegex("( 0 0 0 7 )"));
	}

	@Test
	public void testCase43() {
	    assertFalse(bond.bondRegex("( 0 0 1 ("));
	}

	@Test
	public void testCase44() {
	    assertFalse(bond.bondRegex("( 0 0 1 )"));
	}

	@Test
	public void testCase45() {
	    assertFalse(bond.bondRegex("( 0 0 1 0 0 7 )"));
	}

	@Test
	public void testCase46() {
	    assertFalse(bond.bondRegex("( 0 0 1 0 7 )"));
	}

	@Test
	public void testCase47() {
	    assertFalse(bond.bondRegex("( 0 0 1 7 )"));
	}

	@Test
	public void testCase48() {
	    assertFalse(bond.bondRegex("( 0 0 2 ("));
	}

	@Test
	public void testCase49() {
	    assertFalse(bond.bondRegex("( 0 0 2 )"));
	}

	@Test
	public void testCase50() {
	    assertFalse(bond.bondRegex("( 0 0 2 0 0 7 )"));
	}

	@Test
	public void testCase51() {
	    assertFalse(bond.bondRegex("( 0 0 2 0 7 )"));
	}

	@Test
	public void testCase52() {
	    assertFalse(bond.bondRegex("( 0 0 2 7 )"));
	}

	@Test
	public void testCase53() {
	    assertFalse(bond.bondRegex("( 0 0 3 ("));
	}

	@Test
	public void testCase54() {
	    assertFalse(bond.bondRegex("( 0 0 3 )"));
	}

	@Test
	public void testCase55() {
	    assertFalse(bond.bondRegex("( 0 0 3 0 0 7 )"));
	}

	@Test
	public void testCase56() {
	    assertFalse(bond.bondRegex("( 0 0 3 0 7 )"));
	}

	@Test
	public void testCase57() {
	    assertFalse(bond.bondRegex("( 0 0 3 7 )"));
	}

	@Test
	public void testCase58() {
	    assertFalse(bond.bondRegex("( 0 0 4 ("));
	}

	@Test
	public void testCase59() {
	    assertFalse(bond.bondRegex("( 0 0 4 )"));
	}

	@Test
	public void testCase60() {
	    assertFalse(bond.bondRegex("( 0 0 4 0 0 7 )"));
	}

	@Test
	public void testCase61() {
	    assertFalse(bond.bondRegex("( 0 0 4 0 7 )"));
	}

	@Test
	public void testCase62() {
	    assertFalse(bond.bondRegex("( 0 0 4 7 )"));
	}

	@Test
	public void testCase63() {
	    assertFalse(bond.bondRegex("( 0 0 5 ("));
	}

	@Test
	public void testCase64() {
	    assertFalse(bond.bondRegex("( 0 0 5 )"));
	}

	@Test
	public void testCase65() {
	    assertFalse(bond.bondRegex("( 0 0 5 0 0 7 )"));
	}

	@Test
	public void testCase66() {
	    assertFalse(bond.bondRegex("( 0 0 5 0 7 )"));
	}

	@Test
	public void testCase67() {
	    assertFalse(bond.bondRegex("( 0 0 5 7 )"));
	}

	@Test
	public void testCase68() {
	    assertFalse(bond.bondRegex("( 0 0 6 ("));
	}

	@Test
	public void testCase69() {
	    assertFalse(bond.bondRegex("( 0 0 6 )"));
	}

	@Test
	public void testCase70() {
	    assertFalse(bond.bondRegex("( 0 0 6 0 0 7 )"));
	}

	@Test
	public void testCase71() {
	    assertFalse(bond.bondRegex("( 0 0 6 0 7 )"));
	}

	@Test
	public void testCase72() {
	    assertFalse(bond.bondRegex("( 0 0 6 7 )"));
	}

	@Test
	public void testCase73() {
	    assertFalse(bond.bondRegex("( 0 0 7 ("));
	}

	@Test
	public void testCase74() {
	    assertFalse(bond.bondRegex("( 0 0 7 ( ("));
	}

	@Test
	public void testCase75() {
	    assertTrue(bond.bondRegex("( 0 0 7 ( )"));
	}

	@Test
	public void testCase76() {
	    assertTrue(bond.bondRegex("( 0 0 7 ( 0 0 7 )"));
	}

	@Test
	public void testCase77() {
	    assertTrue(bond.bondRegex("( 0 0 7 ( 0 7 )"));
	}

	@Test
	public void testCase78() {
	    assertTrue(bond.bondRegex("( 0 0 7 ( 7 )"));
	}

	@Test
	public void testCase79() {
	    assertTrue(bond.bondRegex("( 0 0 7 )"));
	}

	@Test
	public void testCase80() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ("));
	}

	@Test
	public void testCase81() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ( ("));
	}

	@Test
	public void testCase82() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ( )"));
	}

	@Test
	public void testCase83() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ( 0 0 7 )"));
	}

	@Test
	public void testCase84() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ( 0 7 )"));
	}

	@Test
	public void testCase85() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ( 7 )"));
	}

	@Test
	public void testCase86() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) )"));
	}

	@Test
	public void testCase87() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ) ("));
	}

	@Test
	public void testCase88() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ) )"));
	}

	@Test
	public void testCase89() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ) 0 0 7 )"));
	}

	@Test
	public void testCase90() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ) 0 7 )"));
	}

	@Test
	public void testCase91() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) ) 7 )"));
	}

	@Test
	public void testCase92() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 0 ("));
	}

	@Test
	public void testCase93() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 0 )"));
	}

	@Test
	public void testCase94() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 0 0 0 7 )"));
	}

	@Test
	public void testCase95() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 0 0 7 )"));
	}

	@Test
	public void testCase96() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 0 7 )"));
	}

	@Test
	public void testCase97() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 1 ("));
	}

	@Test
	public void testCase98() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 1 )"));
	}

	@Test
	public void testCase99() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 1 0 0 7 )"));
	}

	@Test
	public void testCase100() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 1 0 7 )"));
	}

	@Test
	public void testCase101() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 1 7 )"));
	}

	@Test
	public void testCase102() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 2 ("));
	}

	@Test
	public void testCase103() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 2 )"));
	}

	@Test
	public void testCase104() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 2 0 0 7 )"));
	}

	@Test
	public void testCase105() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 2 0 7 )"));
	}

	@Test
	public void testCase106() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 2 7 )"));
	}

	@Test
	public void testCase107() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 3 ("));
	}

	@Test
	public void testCase108() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 3 )"));
	}

	@Test
	public void testCase109() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 3 0 0 7 )"));
	}

	@Test
	public void testCase110() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 3 0 7 )"));
	}

	@Test
	public void testCase111() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 3 7 )"));
	}

	@Test
	public void testCase112() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 4 ("));
	}

	@Test
	public void testCase113() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 4 )"));
	}

	@Test
	public void testCase114() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 4 0 0 7 )"));
	}

	@Test
	public void testCase115() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 4 0 7 )"));
	}

	@Test
	public void testCase116() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 4 7 )"));
	}

	@Test
	public void testCase117() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 5 ("));
	}

	@Test
	public void testCase118() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 5 )"));
	}

	@Test
	public void testCase119() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 5 0 0 7 )"));
	}

	@Test
	public void testCase120() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 5 0 7 )"));
	}

	@Test
	public void testCase121() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 5 7 )"));
	}

	@Test
	public void testCase122() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 6 ("));
	}

	@Test
	public void testCase123() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 6 )"));
	}

	@Test
	public void testCase124() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 6 0 0 7 )"));
	}

	@Test
	public void testCase125() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 6 0 7 )"));
	}

	@Test
	public void testCase126() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 6 7 )"));
	}

	@Test
	public void testCase127() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 7 ("));
	}

	@Test
	public void testCase128() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 7 )"));
	}

	@Test
	public void testCase129() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 7 0 0 7 )"));
	}

	@Test
	public void testCase130() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 7 0 7 )"));
	}

	@Test
	public void testCase131() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 7 7 )"));
	}

	@Test
	public void testCase132() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 8 ("));
	}

	@Test
	public void testCase133() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 8 )"));
	}

	@Test
	public void testCase134() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 8 0 0 7 )"));
	}

	@Test
	public void testCase135() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 8 0 7 )"));
	}

	@Test
	public void testCase136() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 8 7 )"));
	}

	@Test
	public void testCase137() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 9 ("));
	}

	@Test
	public void testCase138() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 9 )"));
	}

	@Test
	public void testCase139() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 9 0 0 7 )"));
	}

	@Test
	public void testCase140() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 9 0 7 )"));
	}

	@Test
	public void testCase141() {
	    assertTrue(bond.bondRegex("( 0 0 7 ) 9 7 )"));
	}

	@Test
	public void testCase142() {
	    assertFalse(bond.bondRegex("( 0 0 7 0 ("));
	}

	@Test
	public void testCase143() {
	    assertTrue(bond.bondRegex("( 0 0 7 0 )"));
	}

	@Test
	public void testCase144() {
	    assertTrue(bond.bondRegex("( 0 0 7 0 0 0 7 )"));
	}

	@Test
	public void testCase145() {
	    assertTrue(bond.bondRegex("( 0 0 7 0 0 7 )"));
	}

	@Test
	public void testCase146() {
	    assertTrue(bond.bondRegex("( 0 0 7 0 7 )"));
	}

	@Test
	public void testCase147() {
	    assertFalse(bond.bondRegex("( 0 0 7 1 ("));
	}

	@Test
	public void testCase148() {
	    assertTrue(bond.bondRegex("( 0 0 7 1 )"));
	}

	@Test
	public void testCase149() {
	    assertTrue(bond.bondRegex("( 0 0 7 1 0 0 7 )"));
	}

	@Test
	public void testCase150() {
	    assertTrue(bond.bondRegex("( 0 0 7 1 0 7 )"));
	}

	@Test
	public void testCase151() {
	    assertTrue(bond.bondRegex("( 0 0 7 1 7 )"));
	}

	@Test
	public void testCase152() {
	    assertFalse(bond.bondRegex("( 0 0 7 2 ("));
	}

	@Test
	public void testCase153() {
	    assertTrue(bond.bondRegex("( 0 0 7 2 )"));
	}

	@Test
	public void testCase154() {
	    assertTrue(bond.bondRegex("( 0 0 7 2 0 0 7 )"));
	}

	@Test
	public void testCase155() {
	    assertTrue(bond.bondRegex("( 0 0 7 2 0 7 )"));
	}

	@Test
	public void testCase156() {
	    assertTrue(bond.bondRegex("( 0 0 7 2 7 )"));
	}

	@Test
	public void testCase157() {
	    assertFalse(bond.bondRegex("( 0 0 7 3 ("));
	}

	@Test
	public void testCase158() {
	    assertTrue(bond.bondRegex("( 0 0 7 3 )"));
	}

	@Test
	public void testCase159() {
	    assertTrue(bond.bondRegex("( 0 0 7 3 0 0 7 )"));
	}

	@Test
	public void testCase160() {
	    assertTrue(bond.bondRegex("( 0 0 7 3 0 7 )"));
	}

	@Test
	public void testCase161() {
	    assertTrue(bond.bondRegex("( 0 0 7 3 7 )"));
	}

	@Test
	public void testCase162() {
	    assertFalse(bond.bondRegex("( 0 0 7 4 ("));
	}

	@Test
	public void testCase163() {
	    assertTrue(bond.bondRegex("( 0 0 7 4 )"));
	}

	@Test
	public void testCase164() {
	    assertTrue(bond.bondRegex("( 0 0 7 4 0 0 7 )"));
	}

	@Test
	public void testCase165() {
	    assertTrue(bond.bondRegex("( 0 0 7 4 0 7 )"));
	}

	@Test
	public void testCase166() {
	    assertTrue(bond.bondRegex("( 0 0 7 4 7 )"));
	}

	@Test
	public void testCase167() {
	    assertFalse(bond.bondRegex("( 0 0 7 5 ("));
	}

	@Test
	public void testCase168() {
	    assertTrue(bond.bondRegex("( 0 0 7 5 )"));
	}

	@Test
	public void testCase169() {
	    assertTrue(bond.bondRegex("( 0 0 7 5 0 0 7 )"));
	}

	@Test
	public void testCase170() {
	    assertTrue(bond.bondRegex("( 0 0 7 5 0 7 )"));
	}

	@Test
	public void testCase171() {
	    assertTrue(bond.bondRegex("( 0 0 7 5 7 )"));
	}

	@Test
	public void testCase172() {
	    assertFalse(bond.bondRegex("( 0 0 7 6 ("));
	}

	@Test
	public void testCase173() {
	    assertTrue(bond.bondRegex("( 0 0 7 6 )"));
	}

	@Test
	public void testCase174() {
	    assertTrue(bond.bondRegex("( 0 0 7 6 0 0 7 )"));
	}

	@Test
	public void testCase175() {
	    assertTrue(bond.bondRegex("( 0 0 7 6 0 7 )"));
	}

	@Test
	public void testCase176() {
	    assertTrue(bond.bondRegex("( 0 0 7 6 7 )"));
	}

	@Test
	public void testCase177() {
	    assertFalse(bond.bondRegex("( 0 0 7 7 ("));
	}

	@Test
	public void testCase178() {
	    assertTrue(bond.bondRegex("( 0 0 7 7 )"));
	}

	@Test
	public void testCase179() {
	    assertTrue(bond.bondRegex("( 0 0 7 7 0 0 7 )"));
	}

	@Test
	public void testCase180() {
	    assertTrue(bond.bondRegex("( 0 0 7 7 0 7 )"));
	}

	@Test
	public void testCase181() {
	    assertTrue(bond.bondRegex("( 0 0 7 7 7 )"));
	}

	@Test
	public void testCase182() {
	    assertFalse(bond.bondRegex("( 0 0 7 8 ("));
	}

	@Test
	public void testCase183() {
	    assertTrue(bond.bondRegex("( 0 0 7 8 )"));
	}

	@Test
	public void testCase184() {
	    assertTrue(bond.bondRegex("( 0 0 7 8 0 0 7 )"));
	}

	@Test
	public void testCase185() {
	    assertTrue(bond.bondRegex("( 0 0 7 8 0 7 )"));
	}

	@Test
	public void testCase186() {
	    assertTrue(bond.bondRegex("( 0 0 7 8 7 )"));
	}

	@Test
	public void testCase187() {
	    assertFalse(bond.bondRegex("( 0 0 7 9 ("));
	}

	@Test
	public void testCase188() {
	    assertTrue(bond.bondRegex("( 0 0 7 9 )"));
	}

	@Test
	public void testCase189() {
	    assertTrue(bond.bondRegex("( 0 0 7 9 0 0 7 )"));
	}

	@Test
	public void testCase190() {
	    assertTrue(bond.bondRegex("( 0 0 7 9 0 7 )"));
	}

	@Test
	public void testCase191() {
	    assertTrue(bond.bondRegex("( 0 0 7 9 7 )"));
	}

	@Test
	public void testCase192() {
	    assertFalse(bond.bondRegex("( 0 0 8 ("));
	}

	@Test
	public void testCase193() {
	    assertFalse(bond.bondRegex("( 0 0 8 )"));
	}

	@Test
	public void testCase194() {
	    assertFalse(bond.bondRegex("( 0 0 8 0 0 7 )"));
	}

	@Test
	public void testCase195() {
	    assertFalse(bond.bondRegex("( 0 0 8 0 7 )"));
	}

	@Test
	public void testCase196() {
	    assertFalse(bond.bondRegex("( 0 0 8 7 )"));
	}

	@Test
	public void testCase197() {
	    assertFalse(bond.bondRegex("( 0 0 9 ("));
	}

	@Test
	public void testCase198() {
	    assertFalse(bond.bondRegex("( 0 0 9 )"));
	}

	@Test
	public void testCase199() {
	    assertFalse(bond.bondRegex("( 0 0 9 0 0 7 )"));
	}

	@Test
	public void testCase200() {
	    assertFalse(bond.bondRegex("( 0 0 9 0 7 )"));
	}

	@Test
	public void testCase201() {
	    assertFalse(bond.bondRegex("( 0 0 9 7 )"));
	}

	@Test
	public void testCase202() {
	    assertFalse(bond.bondRegex("( 0 1 ("));
	}

	@Test
	public void testCase203() {
	    assertFalse(bond.bondRegex("( 0 1 )"));
	}

	@Test
	public void testCase204() {
	    assertFalse(bond.bondRegex("( 0 1 0 0 7 )"));
	}

	@Test
	public void testCase205() {
	    assertFalse(bond.bondRegex("( 0 1 0 7 )"));
	}

	@Test
	public void testCase206() {
	    assertFalse(bond.bondRegex("( 0 1 7 )"));
	}

	@Test
	public void testCase207() {
	    assertFalse(bond.bondRegex("( 0 2 ("));
	}

	@Test
	public void testCase208() {
	    assertFalse(bond.bondRegex("( 0 2 )"));
	}

	@Test
	public void testCase209() {
	    assertFalse(bond.bondRegex("( 0 2 0 0 7 )"));
	}

	@Test
	public void testCase210() {
	    assertFalse(bond.bondRegex("( 0 2 0 7 )"));
	}

	@Test
	public void testCase211() {
	    assertFalse(bond.bondRegex("( 0 2 7 )"));
	}

	@Test
	public void testCase212() {
	    assertFalse(bond.bondRegex("( 0 3 ("));
	}

	@Test
	public void testCase213() {
	    assertFalse(bond.bondRegex("( 0 3 )"));
	}

	@Test
	public void testCase214() {
	    assertFalse(bond.bondRegex("( 0 3 0 0 7 )"));
	}

	@Test
	public void testCase215() {
	    assertFalse(bond.bondRegex("( 0 3 0 7 )"));
	}

	@Test
	public void testCase216() {
	    assertFalse(bond.bondRegex("( 0 3 7 )"));
	}

	@Test
	public void testCase217() {
	    assertFalse(bond.bondRegex("( 0 4 ("));
	}

	@Test
	public void testCase218() {
	    assertFalse(bond.bondRegex("( 0 4 )"));
	}

	@Test
	public void testCase219() {
	    assertFalse(bond.bondRegex("( 0 4 0 0 7 )"));
	}

	@Test
	public void testCase220() {
	    assertFalse(bond.bondRegex("( 0 4 0 7 )"));
	}

	@Test
	public void testCase221() {
	    assertFalse(bond.bondRegex("( 0 4 7 )"));
	}

	@Test
	public void testCase222() {
	    assertFalse(bond.bondRegex("( 0 5 ("));
	}

	@Test
	public void testCase223() {
	    assertFalse(bond.bondRegex("( 0 5 )"));
	}

	@Test
	public void testCase224() {
	    assertFalse(bond.bondRegex("( 0 5 0 0 7 )"));
	}

	@Test
	public void testCase225() {
	    assertFalse(bond.bondRegex("( 0 5 0 7 )"));
	}

	@Test
	public void testCase226() {
	    assertFalse(bond.bondRegex("( 0 5 7 )"));
	}

	@Test
	public void testCase227() {
	    assertFalse(bond.bondRegex("( 0 6 ("));
	}

	@Test
	public void testCase228() {
	    assertFalse(bond.bondRegex("( 0 6 )"));
	}

	@Test
	public void testCase229() {
	    assertFalse(bond.bondRegex("( 0 6 0 0 7 )"));
	}

	@Test
	public void testCase230() {
	    assertFalse(bond.bondRegex("( 0 6 0 7 )"));
	}

	@Test
	public void testCase231() {
	    assertFalse(bond.bondRegex("( 0 6 7 )"));
	}

	@Test
	public void testCase232() {
	    assertFalse(bond.bondRegex("( 0 7 ("));
	}

	@Test
	public void testCase233() {
	    assertFalse(bond.bondRegex("( 0 7 )"));
	}

	@Test
	public void testCase234() {
	    assertFalse(bond.bondRegex("( 0 7 0 0 7 )"));
	}

	@Test
	public void testCase235() {
	    assertFalse(bond.bondRegex("( 0 7 0 7 )"));
	}

	@Test
	public void testCase236() {
	    assertFalse(bond.bondRegex("( 0 7 7 )"));
	}

	@Test
	public void testCase237() {
	    assertFalse(bond.bondRegex("( 0 8 ("));
	}

	@Test
	public void testCase238() {
	    assertFalse(bond.bondRegex("( 0 8 )"));
	}

	@Test
	public void testCase239() {
	    assertFalse(bond.bondRegex("( 0 8 0 0 7 )"));
	}

	@Test
	public void testCase240() {
	    assertFalse(bond.bondRegex("( 0 8 0 7 )"));
	}

	@Test
	public void testCase241() {
	    assertFalse(bond.bondRegex("( 0 8 7 )"));
	}

	@Test
	public void testCase242() {
	    assertFalse(bond.bondRegex("( 0 9 ("));
	}

	@Test
	public void testCase243() {
	    assertFalse(bond.bondRegex("( 0 9 )"));
	}

	@Test
	public void testCase244() {
	    assertFalse(bond.bondRegex("( 0 9 0 0 7 )"));
	}

	@Test
	public void testCase245() {
	    assertFalse(bond.bondRegex("( 0 9 0 7 )"));
	}

	@Test
	public void testCase246() {
	    assertFalse(bond.bondRegex("( 0 9 7 )"));
	}

	@Test
	public void testCase247() {
	    assertFalse(bond.bondRegex("( 1 ("));
	}

	@Test
	public void testCase248() {
	    assertFalse(bond.bondRegex("( 1 )"));
	}

	@Test
	public void testCase249() {
	    assertTrue(bond.bondRegex("( 1 0 0 7 )"));
	}

	@Test
	public void testCase250() {
	    assertFalse(bond.bondRegex("( 1 0 7 )"));
	}

	@Test
	public void testCase251() {
	    assertFalse(bond.bondRegex("( 1 7 )"));
	}

	@Test
	public void testCase252() {
	    assertFalse(bond.bondRegex("( 2 ("));
	}

	@Test
	public void testCase253() {
	    assertFalse(bond.bondRegex("( 2 )"));
	}

	@Test
	public void testCase254() {
	    assertTrue(bond.bondRegex("( 2 0 0 7 )"));
	}

	@Test
	public void testCase255() {
	    assertFalse(bond.bondRegex("( 2 0 7 )"));
	}

	@Test
	public void testCase256() {
	    assertFalse(bond.bondRegex("( 2 7 )"));
	}

	@Test
	public void testCase257() {
	    assertFalse(bond.bondRegex("( 3 ("));
	}

	@Test
	public void testCase258() {
	    assertFalse(bond.bondRegex("( 3 )"));
	}

	@Test
	public void testCase259() {
	    assertTrue(bond.bondRegex("( 3 0 0 7 )"));
	}

	@Test
	public void testCase260() {
	    assertFalse(bond.bondRegex("( 3 0 7 )"));
	}

	@Test
	public void testCase261() {
	    assertFalse(bond.bondRegex("( 3 7 )"));
	}

	@Test
	public void testCase262() {
	    assertFalse(bond.bondRegex("( 4 ("));
	}

	@Test
	public void testCase263() {
	    assertFalse(bond.bondRegex("( 4 )"));
	}

	@Test
	public void testCase264() {
	    assertTrue(bond.bondRegex("( 4 0 0 7 )"));
	}

	@Test
	public void testCase265() {
	    assertFalse(bond.bondRegex("( 4 0 7 )"));
	}

	@Test
	public void testCase266() {
	    assertFalse(bond.bondRegex("( 4 7 )"));
	}

	@Test
	public void testCase267() {
	    assertFalse(bond.bondRegex("( 5 ("));
	}

	@Test
	public void testCase268() {
	    assertFalse(bond.bondRegex("( 5 )"));
	}

	@Test
	public void testCase269() {
	    assertTrue(bond.bondRegex("( 5 0 0 7 )"));
	}

	@Test
	public void testCase270() {
	    assertFalse(bond.bondRegex("( 5 0 7 )"));
	}

	@Test
	public void testCase271() {
	    assertFalse(bond.bondRegex("( 5 7 )"));
	}

	@Test
	public void testCase272() {
	    assertFalse(bond.bondRegex("( 6 ("));
	}

	@Test
	public void testCase273() {
	    assertFalse(bond.bondRegex("( 6 )"));
	}

	@Test
	public void testCase274() {
	    assertTrue(bond.bondRegex("( 6 0 0 7 )"));
	}

	@Test
	public void testCase275() {
	    assertFalse(bond.bondRegex("( 6 0 7 )"));
	}

	@Test
	public void testCase276() {
	    assertFalse(bond.bondRegex("( 6 7 )"));
	}

	@Test
	public void testCase277() {
	    assertFalse(bond.bondRegex("( 7 ("));
	}

	@Test
	public void testCase278() {
	    assertFalse(bond.bondRegex("( 7 )"));
	}

	@Test
	public void testCase279() {
	    assertTrue(bond.bondRegex("( 7 0 0 7 )"));
	}

	@Test
	public void testCase280() {
	    assertFalse(bond.bondRegex("( 7 0 7 )"));
	}

	@Test
	public void testCase281() {
	    assertFalse(bond.bondRegex("( 7 7 )"));
	}

	@Test
	public void testCase282() {
	    assertFalse(bond.bondRegex("( 8 ("));
	}

	@Test
	public void testCase283() {
	    assertFalse(bond.bondRegex("( 8 )"));
	}

	@Test
	public void testCase284() {
	    assertTrue(bond.bondRegex("( 8 0 0 7 )"));
	}

	@Test
	public void testCase285() {
	    assertFalse(bond.bondRegex("( 8 0 7 )"));
	}

	@Test
	public void testCase286() {
	    assertFalse(bond.bondRegex("( 8 7 )"));
	}

	@Test
	public void testCase287() {
	    assertFalse(bond.bondRegex("( 9 ("));
	}

	@Test
	public void testCase288() {
	    assertFalse(bond.bondRegex("( 9 )"));
	}

	@Test
	public void testCase289() {
	    assertTrue(bond.bondRegex("( 9 0 0 7 )"));
	}

	@Test
	public void testCase290() {
	    assertFalse(bond.bondRegex("( 9 0 7 )"));
	}

	@Test
	public void testCase291() {
	    assertFalse(bond.bondRegex("( 9 7 )"));
	}

	@Test
	public void testCase292() {
	    assertFalse(bond.bondRegex(")"));
	}

	@Test
	public void testCase293() {
	    assertFalse(bond.bondRegex(") ("));
	}

	@Test
	public void testCase294() {
	    assertFalse(bond.bondRegex(") )"));
	}

	@Test
	public void testCase295() {
	    assertFalse(bond.bondRegex(") 0 0 7 )"));
	}

	@Test
	public void testCase296() {
	    assertFalse(bond.bondRegex(") 0 7 )"));
	}

	@Test
	public void testCase297() {
	    assertFalse(bond.bondRegex(") 7 )"));
	}

	@Test
	public void testCase298() {
	    assertFalse(bond.bondRegex("0 ("));
	}

	@Test
	public void testCase299() {
	    assertFalse(bond.bondRegex("0 )"));
	}

	@Test
	public void testCase300() {
	    assertFalse(bond.bondRegex("0 0 0 7 )"));
	}

	@Test
	public void testCase301() {
	    assertFalse(bond.bondRegex("0 0 7 )"));
	}

	@Test
	public void testCase302() {
	    assertFalse(bond.bondRegex("0 7 )"));
	}

	@Test
	public void testCase303() {
	    assertFalse(bond.bondRegex("1 ("));
	}

	@Test
	public void testCase304() {
	    assertFalse(bond.bondRegex("1 )"));
	}

	@Test
	public void testCase305() {
	    assertFalse(bond.bondRegex("1 0 0 7 )"));
	}

	@Test
	public void testCase306() {
	    assertFalse(bond.bondRegex("1 0 7 )"));
	}

	@Test
	public void testCase307() {
	    assertFalse(bond.bondRegex("1 7 )"));
	}

	@Test
	public void testCase308() {
	    assertFalse(bond.bondRegex("2 ("));
	}

	@Test
	public void testCase309() {
	    assertFalse(bond.bondRegex("2 )"));
	}

	@Test
	public void testCase310() {
	    assertFalse(bond.bondRegex("2 0 0 7 )"));
	}

	@Test
	public void testCase311() {
	    assertFalse(bond.bondRegex("2 0 7 )"));
	}

	@Test
	public void testCase312() {
	    assertFalse(bond.bondRegex("2 7 )"));
	}

	@Test
	public void testCase313() {
	    assertFalse(bond.bondRegex("3 ("));
	}

	@Test
	public void testCase314() {
	    assertFalse(bond.bondRegex("3 )"));
	}

	@Test
	public void testCase315() {
	    assertFalse(bond.bondRegex("3 0 0 7 )"));
	}

	@Test
	public void testCase316() {
	    assertFalse(bond.bondRegex("3 0 7 )"));
	}

	@Test
	public void testCase317() {
	    assertFalse(bond.bondRegex("3 7 )"));
	}

	@Test
	public void testCase318() {
	    assertFalse(bond.bondRegex("4 ("));
	}

	@Test
	public void testCase319() {
	    assertFalse(bond.bondRegex("4 )"));
	}

	@Test
	public void testCase320() {
	    assertFalse(bond.bondRegex("4 0 0 7 )"));
	}

	@Test
	public void testCase321() {
	    assertFalse(bond.bondRegex("4 0 7 )"));
	}

	@Test
	public void testCase322() {
	    assertFalse(bond.bondRegex("4 7 )"));
	}

	@Test
	public void testCase323() {
	    assertFalse(bond.bondRegex("5 ("));
	}

	@Test
	public void testCase324() {
	    assertFalse(bond.bondRegex("5 )"));
	}

	@Test
	public void testCase325() {
	    assertFalse(bond.bondRegex("5 0 0 7 )"));
	}

	@Test
	public void testCase326() {
	    assertFalse(bond.bondRegex("5 0 7 )"));
	}

	@Test
	public void testCase327() {
	    assertFalse(bond.bondRegex("5 7 )"));
	}

	@Test
	public void testCase328() {
	    assertFalse(bond.bondRegex("6 ("));
	}

	@Test
	public void testCase329() {
	    assertFalse(bond.bondRegex("6 )"));
	}

	@Test
	public void testCase330() {
	    assertFalse(bond.bondRegex("6 0 0 7 )"));
	}

	@Test
	public void testCase331() {
	    assertFalse(bond.bondRegex("6 0 7 )"));
	}

	@Test
	public void testCase332() {
	    assertFalse(bond.bondRegex("6 7 )"));
	}

	@Test
	public void testCase333() {
	    assertFalse(bond.bondRegex("7 ("));
	}

	@Test
	public void testCase334() {
	    assertFalse(bond.bondRegex("7 )"));
	}

	@Test
	public void testCase335() {
	    assertFalse(bond.bondRegex("7 0 0 7 )"));
	}

	@Test
	public void testCase336() {
	    assertFalse(bond.bondRegex("7 0 7 )"));
	}

	@Test
	public void testCase337() {
	    assertFalse(bond.bondRegex("7 7 )"));
	}

	@Test
	public void testCase338() {
	    assertFalse(bond.bondRegex("8 ("));
	}

	@Test
	public void testCase339() {
	    assertFalse(bond.bondRegex("8 )"));
	}

	@Test
	public void testCase340() {
	    assertFalse(bond.bondRegex("8 0 0 7 )"));
	}

	@Test
	public void testCase341() {
	    assertFalse(bond.bondRegex("8 0 7 )"));
	}

	@Test
	public void testCase342() {
	    assertFalse(bond.bondRegex("8 7 )"));
	}

	@Test
	public void testCase343() {
	    assertFalse(bond.bondRegex("9 ("));
	}

	@Test
	public void testCase344() {
	    assertFalse(bond.bondRegex("9 )"));
	}

	@Test
	public void testCase345() {
	    assertFalse(bond.bondRegex("9 0 0 7 )"));
	}

	@Test
	public void testCase346() {
	    assertFalse(bond.bondRegex("9 0 7 )"));
	}

	@Test
	public void testCase347() {
	    assertFalse(bond.bondRegex("9 7 )"));
	}




}
